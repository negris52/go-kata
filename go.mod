module gitlab.com/negris52/go-kata

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/essentialkaos/translit v2.0.3+incompatible
	gitlab.com/negris52/greet v0.0.0-20230104175551-6f57f6d0d41b
)

require (
	github.com/kr/pretty v0.3.1 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	pkg.re/essentialkaos/check.v1 v1.2.0 // indirect
)
