package main

import "testing"

type operationTest struct {
	arg1, agr2, expected int
}

var sumTests = []operationTest{
	operationTest{2, 3, 5},
	operationTest{7, 13, 20},
	operationTest{1, 1, 2},
	operationTest{12, 23, 35},
	operationTest{22, 33, 55},
	operationTest{102, 143, 245},
	operationTest{282, 394, 676},
	operationTest{1002, 1387, 2389},
}

var divTests = []operationTest{
	operationTest{4, 2, 2},
	operationTest{8, 2, 4},
	operationTest{40, 5, 8},
	operationTest{99, 3, 33},
	operationTest{100, 5, 20},
	operationTest{250, 25, 10},
	operationTest{5000, 10, 500},
	operationTest{10000, 4, 2500},
}

var averTests = []operationTest{
	operationTest{12, 6, 9},
	operationTest{22, 44, 33},
	operationTest{40, 6, 23},
	operationTest{99, 3, 51},
	operationTest{6, 100, 53},
	operationTest{250, 50, 150},
	operationTest{5000, 1000, 3000},
	operationTest{10000, 6000, 8000},
}

func TestSum(t *testing.T) {
	for _, test := range sumTests {
		if output := Sum(float64(test.arg1), float64(test.agr2)); output != float64(test.expected) {
			t.Errorf("Output %v not equal to expected %v", output, test.expected)
		}
	}
}

func TestDivide(t *testing.T) {
	for _, test := range divTests {
		if output := Divide(float64(test.arg1), float64(test.agr2)); output != float64(test.expected) {
			t.Errorf("Output %v not equal to expected %v", output, test.expected)
		}
	}
}

func TestAverage(t *testing.T) {
	for _, test := range averTests {
		if output := Average(float64(test.arg1), float64(test.agr2)); output != float64(test.expected) {
			t.Errorf("Output %v not equal to expected %v", output, test.expected)
		}
	}
}
