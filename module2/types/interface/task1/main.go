package main

import (
	"fmt"
	"reflect"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch reflect.ValueOf(r).Elem() {
	case reflect.ValueOf(nil):
		fmt.Println("Success!")
	}
}
