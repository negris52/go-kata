package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		// сюда впишите ваши остальные 12 структур
		{
			Name:  "https://github.com/flutter/flutter",
			Stars: 148000,
		},
		{
			Name:  "https://github.com/facebook/react-native",
			Stars: 107000,
		},
		{
			Name:  "https://github.com/Hack-with-Github/Awesome-Hacking",
			Stars: 59700,
		},
		{
			Name:  "https://github.com/google/material-design-icons",
			Stars: 47000,
		},
		{
			Name:  "https://github.com/wasabeef/awesome-android-ui",
			Stars: 45000,
		},
		{
			Name:  "https://github.com/square/okhttp",
			Stars: 43300,
		},
		{
			Name:  "https://github.com/android/architecture-samples",
			Stars: 42000,
		},
		{
			Name:  "https://github.com/square/retrofit",
			Stars: 40900,
		},
		{
			Name:  "https://github.com/dcloudio/uni-app",
			Stars: 37500,
		},
		{
			Name:  "https://github.com/fastlane/fastlane",
			Stars: 36200,
		},
		{
			Name:  "https://github.com/laurent22/joplin",
			Stars: 33600,
		},
		{
			Name:  "https://github.com/bumptech/glide",
			Stars: 33200,
		},
	}
	// в цикле запишите в map
	projectsMap := make(map[string]Project)
	for _, v := range projects {
		projectsMap[v.Name] = v
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, v := range projectsMap {
		fmt.Println(v)
	}
}
