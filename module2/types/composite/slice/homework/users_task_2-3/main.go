package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	var newUsers []User
	for _, v := range users {
		if v.Age <= 40 {
			newUsers = append(newUsers, v)
		}
	}
	fmt.Println("default users:", users)
	users = newUsers
	fmt.Println("removed all 40+ years old users:", users)
	//remove first user
	users = append([]User{}, users[1:]...)
	//remove last user
	users = append([]User{}, users[:len(users)-1]...)
	fmt.Println("removed first and last users:", users)
}
