package main

import (
	"math/rand"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

type User_test struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product_test
}

type Product_test struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func MapUserProducts_test(users []User_test, products []Product_test) []User_test {
	// Проинициализируйте карту продуктов по айди пользователей
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}
	return users
}

func MapUserProducts2_test(users []User_test, products []Product_test) []User_test {
	// Проинициализируйте карту продуктов по айди пользователей
	prodmap := make(map[int64][]Product_test, 100)
	for _, product := range products { // избавьтесь от цикла в цикле
		prodmap[product.UserID] = append(prodmap[product.UserID], product)
	}
	for i := 1; i < 100; i++ {
		users[i].Products = prodmap[int64(i)]
	}

	return users
}

func genProducts_test() []Product_test {
	products := make([]Product_test, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100) + 1)
		products[i] = product
	}

	return products
}

func genUsers_test() []User_test {
	users := make([]User_test, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}
	return users
}

func BenchmarkSample(b *testing.B) {
	users := genUsers_test()
	//fmt.Printf("%+v", users)
	products := genProducts_test()
	//fmt.Printf("%+v", products)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts_test(users, products)
	}
}

func BenchmarkSample2(b *testing.B) {
	users := genUsers_test()
	//fmt.Printf("%+v", users)
	products := genProducts_test()
	//fmt.Printf("%+v", products)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2_test(users, products)
	}
}
