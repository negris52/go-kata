package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	bb := new(bytes.Buffer)
	// запишите данные в буфер
	for _, str := range data {
		bb.WriteString(str + "\n")
	}
	fmt.Println(bb)
	// создайте файл
	file, err := os.Create("/tmp/example.txt")
	check(err)
	defer file.Close()
	// запишите данные в файл
	str1 := strings.Split(bb.String(), "/n")
	for _, str := range str1 {
		_, err = file.WriteString(str)
		check(err)
	}
	// прочтите данные в новый буфер
	bb2 := new(bytes.Buffer)
	dat, err := os.ReadFile("/tmp/example.txt")
	check(err)
	bb2.WriteString(string(dat))
	fmt.Println(bb2.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
