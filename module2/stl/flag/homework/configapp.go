package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

type Config struct {
	AppName    string
	Production bool
}

func main() {
	argsProg := os.Args[2:]
	var config_path string
	if len(argsProg) == 0 {
		config_path = fmt.Sprintf("%s/%s", SrcDirPath(), "config.json")
	} else {
		config_path = fmt.Sprintf("%s/%s", SrcDirPath(), argsProg[0])
	}
	plan, err1 := os.ReadFile(config_path)
	check(err1)
	var data Config
	err2 := json.Unmarshal(plan, &data)
	check(err2)
	showConfig := flag.Bool("conf", false, "display json config setting")
	flag.Parse()

	if *showConfig {
		fmt.Printf("Production: %v\n", data.Production)
		fmt.Printf("AppName: %v\n", data.AppName)
	}
}

func SrcDirPath() string {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		fmt.Println("Unable to get the current filename")
	}
	dirname := filepath.Dir(filename)
	return dirname
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
