package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./module2/stl/database/gopher.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	check(err)
	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Den", "Brown")
	check(err)
	rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstname, lastname string

	for rows.Next() {
		_ = rows.Scan(&id, &firstname, &lastname)
		fmt.Printf("%d: %s %s\n", id, firstname, lastname)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
