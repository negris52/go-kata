package main

import (
	"fmt"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func main() {
	p := Person{"Max", 34, 20.00000065}
	fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now.", name, age, money)
}
