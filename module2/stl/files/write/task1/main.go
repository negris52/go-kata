package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	file, err := os.Create("/tmp/dat")
	check(err)
	defer file.Close()
	_, err = file.WriteString(name)
	check(err)
	fmt.Printf("Hello %s\n", name)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
