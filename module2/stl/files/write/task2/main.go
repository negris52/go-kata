package main

import (
	"os"

	"github.com/essentialkaos/translit"
)

func main() {
	in, err := os.ReadFile("/tmp/example.txt")
	check(err)
	engtxt := translit.EncodeToBGN(string(in))
	out, err := os.Create("/tmp/example.processed.txt")
	check(err)
	defer out.Close()
	_, err = out.WriteString(engtxt)
	check(err)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
